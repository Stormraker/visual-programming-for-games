// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisProgCharacter.h"
#include "ThirdPersonInteractorCharacter.generated.h"

class UInteractablesManagementComponent;
class USphereComponent;

/**
 * 
 */
UCLASS()
class VISPROG_API AThirdPersonInteractorCharacter : public AVisProgCharacter
{
	GENERATED_BODY()

public:
	AThirdPersonInteractorCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	UPROPERTY()
	USphereComponent* m_interactionRange;
	UPROPERTY()
	UInteractablesManagementComponent* m_interactablesManager;
	
};
