// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Project includes
#include "InteractionSystem/Interactable.h"

// UE4 includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SimpleInteractable.generated.h"

class UBoolStateComponent;

UENUM()
enum class SimpleInteractableAttitude
{
	Blessed UMETA("Blessed(light colored)"),
	Cursed UMETA("Cursed(dark colored)")
};

UENUM()
enum class DissolveState
{
	Vanish		UMETA("object vanishes"),
	Reveal		UMETA("object reveals"),
	Completed	UMETA("object modification completed"),
	Stay		UMETA("object doesn't change")
};

UCLASS()
class VISPROG_API ASimpleInteractable : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ASimpleInteractable();

	// IInteractable interface
	void Interact() override final;
	// End IInteractable interface

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
		UBoolStateComponent* m_interactableState;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* m_interactableMesh;
	UPROPERTY(EditAnywhere)
		SimpleInteractableAttitude m_attitude;
	UPROPERTY()
		DissolveState m_dissolveState;
	UPROPERTY()
		float m_dissolveAmount;
};
