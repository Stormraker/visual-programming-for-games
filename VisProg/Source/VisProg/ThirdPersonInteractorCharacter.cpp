// Fill out your copyright notice in the Description page of Project Settings.


#include "ThirdPersonInteractorCharacter.h"
#include "InteractionSystem/InteractablesManagementComponent.h"
#include "Components/SphereComponent.h"

AThirdPersonInteractorCharacter::AThirdPersonInteractorCharacter()
	: AVisProgCharacter()
	, m_interactionRange(CreateDefaultSubobject<USphereComponent>(TEXT("InteractionRange")))
	, m_interactablesManager(CreateDefaultSubobject<UInteractablesManagementComponent>(TEXT("InteractablesManager")))
{
	m_interactionRange->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	m_interactionRange->SetRelativeLocation({40.f, 0.f, 40.f});
	m_interactionRange->SetSphereRadius(75.f);

	m_interactionRange->SetGenerateOverlapEvents(true);
}

void AThirdPersonInteractorCharacter::BeginPlay()
{
	Super::BeginPlay();

	m_interactionRange->OnComponentBeginOverlap.AddDynamic(m_interactablesManager, &UInteractablesManagementComponent::OnActorEnterRange);
	m_interactionRange->OnComponentEndOverlap.AddDynamic(m_interactablesManager, &UInteractablesManagementComponent::OnActorLeaveRange);
}

void AThirdPersonInteractorCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, m_interactablesManager, &UInteractablesManagementComponent::OnInteract);
}
