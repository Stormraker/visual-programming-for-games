// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleInteractable.h"
#include "InteractionSystem/BoolStateComponent.h"

// Sets default values
ASimpleInteractable::ASimpleInteractable()
	: AActor()
	, m_interactableState(CreateDefaultSubobject<UBoolStateComponent>(TEXT("SimpleInteractableState")))
	, m_interactableMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SimpleInteractableMesh")))
	, m_attitude(SimpleInteractableAttitude::Blessed)
	, m_dissolveState(DissolveState::Stay)
	, m_dissolveAmount(m_attitude == SimpleInteractableAttitude::Blessed ? .0f : 1.0f)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASimpleInteractable::BeginPlay()
{
	Super::BeginPlay();
	

	//initialization of room obstacles
	m_interactableState->SetState(m_attitude == SimpleInteractableAttitude::Cursed);
	m_dissolveAmount = (m_attitude == SimpleInteractableAttitude::Cursed) ? .0f : 1.0f;

	m_interactableMesh->SetScalarParameterValueOnMaterials("dissolveAmount", m_dissolveAmount);
	SetActorEnableCollision(m_interactableState->GetState());
}

// Called every frame
void ASimpleInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	static const float dissolveDuration = 2.0f;
	float dissolveAmountDelta = DeltaTime / dissolveDuration;

	//checking the dissolve state and dissolving the material
	if (m_dissolveState == DissolveState::Reveal && m_dissolveAmount > .0f)
	{
		m_dissolveAmount -= dissolveAmountDelta;
		m_interactableMesh->SetScalarParameterValueOnMaterials("dissolveAmount", m_dissolveAmount);

		//checking if modification is completed and setting DissolveState
		if (m_dissolveAmount <= .0f) { m_dissolveState = DissolveState::Completed; }
	}
	else if (m_dissolveState == DissolveState::Vanish && m_dissolveAmount < 1.0f)
	{
		m_dissolveAmount += dissolveAmountDelta;
		m_interactableMesh->SetScalarParameterValueOnMaterials("dissolveAmount", m_dissolveAmount);

		//checking if modification is completed and setting DissolveState
		if (m_dissolveAmount >= 1.0f) { m_dissolveState = DissolveState::Completed; }
	}
	else if(m_dissolveState == DissolveState::Completed)
	{
		//DissolveState:Stay for less operations each tick
		m_dissolveState = DissolveState::Stay;
		this->SetActorEnableCollision(m_interactableState->GetState());
	}
}

void ASimpleInteractable::Interact()
{
	//setting the dissolve state with dissolve amount -> dissolveAmount = 0 means it is revealed, 1 means vanished
	if (m_interactableState->GetState())
	{
		m_dissolveState = DissolveState::Vanish;
	}
	else
	{
		m_dissolveState = DissolveState::Reveal;
	}

	m_interactableState->ToggleState();

	this->SetActorEnableCollision(true);
}
