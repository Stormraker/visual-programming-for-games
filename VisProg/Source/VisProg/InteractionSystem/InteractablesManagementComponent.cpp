// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractablesManagementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

// Sets default values for this component's properties
UInteractablesManagementComponent::UInteractablesManagementComponent()
	: m_interactablesInRange()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UInteractablesManagementComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UInteractablesManagementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInteractablesManagementComponent::OnActorEnterRange(class UPrimitiveComponent* Comp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
	{
		m_interactablesInRange.AddUnique(OtherActor);
		UE_LOG(LogTemp, Log, TEXT("Added object to interactables in range! (#Interactables: %d)"), m_interactablesInRange.Num());
	}
}

void UInteractablesManagementComponent::OnActorLeaveRange(class UPrimitiveComponent* Comp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (m_interactablesInRange.Remove(OtherActor))
	{
		UE_LOG(LogTemp, Log, TEXT("Removed object from interactables in range!"));
	}
}

void UInteractablesManagementComponent::OnInteract()
{
	for (auto& interactable : m_interactablesInRange)
	{
		interactable->Interact();
	}
}

