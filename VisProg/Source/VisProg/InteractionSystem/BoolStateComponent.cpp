// Fill out your copyright notice in the Description page of Project Settings.


#include "BoolStateComponent.h"

// Sets default values for this component's properties
UBoolStateComponent::UBoolStateComponent()
	: UActorComponent()
	, m_state(false)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UBoolStateComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBoolStateComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBoolStateComponent::SetState(bool state)
{
	m_state = state;
}

bool UBoolStateComponent::GetState() const
{
	return m_state;
}

bool UBoolStateComponent::ToggleState()
{
	m_state = !m_state;

	return m_state;
}
