// Fill out your copyright notice in the Description page of Project Settings.


#include "ToggleButton.h"
#include "InteractionSystem/BoolStateComponent.h"

// Sets default values
AToggleButton::AToggleButton()
	: m_root(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")))
	, m_buttonMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh")))
	, m_buttonFrame(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonFrame")))
	, m_buttonOnState(CreateDefaultSubobject<UBoolStateComponent>(TEXT("ButtonOn")))
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = m_root;
}

// Called when the game starts or when spawned
void AToggleButton::BeginPlay()
{
	Super::BeginPlay();

	m_buttonMesh->SetScalarParameterValueOnMaterials("bIsEnabled", .0f);
}

// Called every frame
void AToggleButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AToggleButton::Interact()
{
	m_buttonOnState->ToggleState();
	m_buttonMesh->SetScalarParameterValueOnMaterials("bIsEnabled", (m_buttonOnState->GetState() ? 1.0f : .0f));
	UE_LOG(LogTemp, Log, TEXT("Button Toggled! new state: %s"), (m_buttonOnState->GetState() ? TEXT("true") : TEXT("false")));
	OnButtonPressed.Broadcast();
}
