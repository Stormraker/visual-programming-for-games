// Copyright Epic Games, Inc. All Rights Reserved.

#include "VisProgGameMode.h"
#include "VisProgCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVisProgGameMode::AVisProgGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
