// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MagicRoom.generated.h"

class ASimpleInteractable;
class IInteractable;

UCLASS()
class VISPROG_API AMagicRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMagicRoom();

	UFUNCTION(BlueprintCallable)
	void ModifyAppearance();

	UFUNCTION(BlueprintCallable)
	bool GetIsRoomPale();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UFUNCTION()
	void ModifyWalls();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
		USceneComponent* m_root;
	UPROPERTY(EditAnywhere)
		TArray<AActor*> m_walls;
	UPROPERTY(EditAnywhere)
		TArray<ASimpleInteractable*> m_obstacles;
	UPROPERTY()
		bool m_isRoomPale;
};
