// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VisProg : ModuleRules
{
	public VisProg(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
