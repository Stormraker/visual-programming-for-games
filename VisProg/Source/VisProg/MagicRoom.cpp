// Fill out your copyright notice in the Description page of Project Settings.


#include "MagicRoom.h"
#include "InteractionSystem/Interactable.h"
#include "SimpleInteractable.h"

// Sets default values
AMagicRoom::AMagicRoom()
	: AActor()
	, m_root(CreateDefaultSubobject<USceneComponent>(TEXT("RootRoom")))
	, m_walls()
	, m_obstacles()
	, m_isRoomPale(true)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMagicRoom::BeginPlay()
{
	Super::BeginPlay();

	ModifyWalls();
}

// Called every frame
void AMagicRoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagicRoom::ModifyAppearance()
{
	m_isRoomPale = !m_isRoomPale;

	ModifyWalls();

	for (auto& obstacle : m_obstacles)
	{
		obstacle->Interact();
	}

}

void AMagicRoom::ModifyWalls()
{
	for (auto& wall : m_walls)
	{
		TArray<UStaticMeshComponent*> wallMeshComponents;
		wall->GetComponents<UStaticMeshComponent>(wallMeshComponents);
		if (wallMeshComponents.Num() != 0)
		{
			wallMeshComponents[0]->SetScalarParameterValueOnMaterials("bIsEnabled", m_isRoomPale ? 1.0f : 0.0f);
		}
	}
}

bool AMagicRoom::GetIsRoomPale()
{
	return m_isRoomPale;
}

