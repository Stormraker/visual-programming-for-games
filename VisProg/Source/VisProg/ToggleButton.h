// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Project includes
#include "InteractionSystem/Interactable.h"

// UE includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ToggleButton.generated.h"

class UBoolStateComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FButtonPressDelegate);

UCLASS()
class VISPROG_API AToggleButton : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AToggleButton();

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override final;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// IInteractable interface
	void Interact() override;
	// End IInteractable interface

private:
	UPROPERTY()
		USceneComponent* m_root;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* m_buttonMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* m_buttonFrame;
	UPROPERTY(VisibleAnywhere)
		UBoolStateComponent* m_buttonOnState;
	UPROPERTY(BlueprintAssignable, Category = "ToggleButton")
		FButtonPressDelegate OnButtonPressed;
};
